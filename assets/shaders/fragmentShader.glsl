#version 330 core
in vec2 fTexCoord;
in vec3 fNormal;
in vec3 fragPos;
in vec4 fDirectionalLightSpacePos;

const int MAX_POINT_LIGHTS = 3;
const int MAX_SPOT_LIGHTS = 3;

out vec4 FragColor; 

uniform vec3 fColor;
uniform sampler2D fTextureSampler;
uniform sampler2D directionalShadowMap;
uniform bool displayShadows;

// base light information
struct Light { 
    vec3 colour;
    float ambientIntensity;
    float diffuseIntensity; 
};

struct DirectionalLight {
    Light base;    
    vec3 direction;
};

struct PointLight {
    Light base;    
    vec3 position;
    float constant;
    float linear;
    float exponent;
};

struct SpotLight {
    PointLight base;
    vec3 direction;
    float edge;
};

struct Material {
    float specularIntensity;
    float shininess;
};

uniform int pointLightCount;
uniform int spotLightCount;

uniform DirectionalLight directionalLight;
uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform SpotLight spotLights[MAX_SPOT_LIGHTS];
uniform Material material;

// cameraPosition
uniform vec3 eyePosition;

// 0 is false
uniform bool isTextured = false;

float CalcDirectionalShadowFactor(DirectionalLight light) {
    vec3 projCoords = fDirectionalLightSpacePos.xyz / fDirectionalLightSpacePos.w;
    projCoords = (projCoords * 0.5) + 0.5;

    // float closestDepth = texture(directionalShadowMap, projCoords.xy).r;
    float currentDepth = projCoords.z;
    
    // bias function 
    float bias = max(0.01 * (1-dot(normalize(fNormal), normalize(light.direction))) , 0.005);

    vec2 texelSize = 1.0 / textureSize(directionalShadowMap, 0);
    
    // form of antialiasing.
    float shadow = 0; 
    for (int x = -1; x <= 1; x++) {
        for (int y = -1; y <= 1; y++) {
            float pcfDepth = texture(directionalShadowMap, projCoords.xy + vec2(x,y)*texelSize).r;
            shadow += currentDepth - bias > pcfDepth? 1.0 : 0.0;
        }
    }
    shadow /= 9.0; 
    // 1.0 is full shadow
    // float shadow = currentDepth - bias > closestDepth? 1.0 : 0.0;
    if (projCoords.z > 1.f) {
        shadow = 0.f;
    }
    return shadow;
}

vec4 CalcLightByDirection(Light light, vec3 direction, float shadowFactor) {
    vec4 ambientColour = vec4(light.colour, 1.0f) * light.ambientIntensity;
    
    // A.B = |A|*|B|*cos(Theta)
    float diffuseFactor = clamp(dot(normalize(fNormal), normalize(-direction)), 0, 1.0);
    vec4 diffuseColour = vec4(light.colour, 1.0f) * light.diffuseIntensity * diffuseFactor;
    

    vec4 specularColour = vec4(0.f, 0.f, 0.f, 0.f);
    
    if(diffuseFactor > 0.f) {
        vec3 fragToEye = normalize(eyePosition - fragPos);
        vec3 reflectedVertex = normalize(reflect(direction, normalize(fNormal)));
        float specularFactor = dot(fragToEye, reflectedVertex);
        if (specularFactor > 0.f) {
            specularFactor = pow(specularFactor, material.shininess);
            specularColour = vec4(light.colour * material.specularIntensity * specularFactor, 1.0);
        }
    }
    return (ambientColour + (1.f - shadowFactor) * (diffuseColour + specularColour));
}

vec4 CalcDirectionalLight() {
    float shadowFactor = 0;
    if (displayShadows)
        shadowFactor = CalcDirectionalShadowFactor(directionalLight);

    return CalcLightByDirection(directionalLight.base, directionalLight.direction, shadowFactor);
}

vec4 CalcPointLight(PointLight pointLight) {
     // compute direction
        vec3 direction = fragPos - pointLight.position;
        // compute distance for the attenuation
        float distance = length(direction);
        // normalize direction
        direction = normalize(direction);
        // for this specific fragment, calculate the light by the direction
        vec4 colour = CalcLightByDirection(pointLight.base, direction, 0.f);
        // quadratic function for the attenuation.
        float attenuation = pointLight.exponent*distance*distance +
            pointLight.linear * distance +
            pointLight.constant;

        return (colour / attenuation);
}

vec4 CalcSpotLight(SpotLight spotLight) {
    vec3 rayDirection = normalize(fragPos - spotLight.base.position);
    float slFactor = dot(rayDirection, normalize(spotLight.direction));
    
    
    if (slFactor > spotLight.edge) {
        vec4 colour = CalcPointLight(spotLight.base);
        return colour * (1.f - (1.f - slFactor) * (1.f/(1.f - spotLight.edge)));
    } else {
        //  vec3 direction = fragPos - spotLight.base.position;
        // // compute distance for the attenuation
        // float distance = length(direction);
        // float attenuation = spotLight.base.exponent*distance*distance +
        //     spotLight.base.linear * distance +
        //     spotLight.base.constant;
        // return (spotLight.base.base.ambientIntensity * vec4(spotLight.base.base.colour, 1.f)) / attenuation;
        return vec4(0,0,0,0);
    }
}

vec4 CalcSpotLights() {
    vec4 totalColour = vec4(0,0,0,0);
    for (int i = 0; i<spotLightCount; i++) {
        totalColour += CalcSpotLight(spotLights[i]);
    }
    return totalColour;
}

vec4 CalcPointLights() {
    vec4 totalColour = vec4(0,0,0,0);
    for (int i = 0; i<pointLightCount; i++) {
        totalColour += CalcPointLight(pointLights[i]) ;
    }
    
    return totalColour;
}

void main() {
     if (isTextured)
        FragColor = texture(fTextureSampler, fTexCoord);
    else
        FragColor = vec4(fColor, 1.0);
    
    // calculate the directional light
    vec4 finalColour = CalcDirectionalLight();
    
    // calculate the point lights
    finalColour += CalcPointLights();
    finalColour += CalcSpotLights();
    FragColor = FragColor * finalColour;
}