#version 330
layout (location = 0) in vec3 aPos;

uniform mat4 model;
uniform mat4 directionalLightSpaceTransform; //projection * view

void main() {
    gl_Position = directionalLightSpaceTransform * model * vec4(aPos, 1.f);
}
