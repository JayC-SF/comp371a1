#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec2 vTexCoord;
layout (location = 2) in vec3 vNormal;

uniform mat4 model = mat4(1.0); //default value if not assigned.
uniform mat4 view = mat4(1.0);
uniform mat4 projection = mat4(1.0);
uniform mat4 directionalLightSpaceTransform; //projection * view

out vec2 fTexCoord;
out vec3 fNormal;
out vec3 fragPos;
out vec4 fDirectionalLightSpacePos;

void main(){
    // position in light space view and perspective
    fDirectionalLightSpacePos = directionalLightSpaceTransform * model * vec4(aPos, 1.f);

    gl_Position = projection * view * model * vec4(aPos, 1.0);
    fTexCoord = vTexCoord;
    fNormal = mat3(transpose(inverse(model))) * vNormal;
    fragPos = (model * vec4(aPos, 1.0)).xyz;
}