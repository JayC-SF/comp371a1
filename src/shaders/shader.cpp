#include "shader.h"
#include <iostream>
#include <vector>

// PUBLIC

Shader::Shader(const char * pVertexFileLocation, const char * pFragmentFileLocation){
    CreateFromFiles(pVertexFileLocation, pFragmentFileLocation);
	aPointLightCount = 0;
}

void Shader::CreateFromString(const char* vertexCode, const char* fragmentCode) {
	CompileShader(vertexCode, fragmentCode);
}

// 
void Shader::CreateFromFiles(const char* vertexLocation, const char* fragmentLocation) {
	std::string vertexString = ReadFile(vertexLocation);
	std::string fragmentString = ReadFile(fragmentLocation);
	const char* vertexCode = vertexString.c_str();
	const char* fragmentCode = fragmentString.c_str();

	CompileShader(vertexCode, fragmentCode);
}

// 
std::string Shader::ReadFile(const char* fileLocation) {
	std::string content;
	std::ifstream fileStream(fileLocation, std::ios::in);

	if (!fileStream.is_open()) {
		printf("Failed to read %s! File doesn't exist.", fileLocation);
		return "";
	}

	std::string line = "";
	while (!fileStream.eof())
	{
		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

GLuint Shader::GetUniformLocation(const char * name) {
    return glGetUniformLocation(aShaderProgramId, name);
}

// 
GLuint Shader::GetColorLocation() {
    return aColorLocation;
}

// 
GLuint Shader::GetModelLocation() {
    return aModelMatrixLocation;
}

// 
GLuint Shader::GetViewLocation() {
    return aViewMatrixLocation;
}

// 
GLuint Shader::GetProjectionLocation() {
    return aProjectionMatrixLocation;
}

GLuint Shader::GetAmbientIntensityLocation() {
	return aDirectionalLightLocation.aAmbientIntensityLocation;
}
GLuint Shader::GetAmbientColorLocation() {
	return aDirectionalLightLocation.aAmbientColorLocation;
}
GLuint Shader::GetDiffuseIntensityLocation() {
	return aDirectionalLightLocation.aDiffuseIntensityLocation;
}
GLuint Shader::GetDirectionLocation() {
	return aDirectionalLightLocation.aDirectionLocation;
}
GLuint Shader::GetSpecularIntensityLocation() {
	return aSpecularIntensityLocation;
}
GLuint Shader::GetShininessLocation() {
	return aShininessLocation;
}
GLuint Shader::GetEyePositionLocation() {
	return aEyePositionLocation;
}

// 
void Shader::SetColor(vec3 pColor) {
    aColor = pColor;
    SetUniform3fv(aColorLocation, pColor);
}

// 
void Shader::SetModelMatrix(mat4 pModelMatrix) {
    aModelMatrix = pModelMatrix;
    SetUniformMatrix4fv(aModelMatrixLocation, &aModelMatrix[0][0]);
}

// 
void Shader::SetViewMatrix(mat4 pViewMatrix) {
    aViewMatrix = pViewMatrix;
    SetUniformMatrix4fv(aViewMatrixLocation, &aViewMatrix[0][0]);
}

// 
void Shader::SetProjectionMatrix(mat4 pProjectionMatrix) {
    aProjectionMatrix = pProjectionMatrix;
    SetUniformMatrix4fv(aProjectionMatrixLocation, &aProjectionMatrix[0][0]);
}

// 
void Shader::SetIsTextured(bool pIsTextured) {
    aIsTextured = pIsTextured;
    SetUniform1i(aIsTexturedLocation, aIsTextured);
}


void Shader::SetEyePosition(vec3 pPos) {
	SetUniform3fv(aEyePositionLocation, pPos);
}

void Shader::SetDirectionalLight(DirectionalLight * pDirectionalLight) {
	pDirectionalLight->UseLight(
		aDirectionalLightLocation.aAmbientIntensityLocation,
		aDirectionalLightLocation.aAmbientColorLocation,
		aDirectionalLightLocation.aDiffuseIntensityLocation,
		aDirectionalLightLocation.aDirectionLocation);
}

void Shader::SetPointLights(vector<PointLight> & pPointLights) {
	unsigned int lightCount = pPointLights.size() > MAX_POINT_LIGHTS? MAX_POINT_LIGHTS : pPointLights.size();
	SetUniform1i(aPointLightCountLocation, lightCount);

	for (uint i = 0; i<lightCount; i++) {
		// GLuint pAmbientIntensityLocation
		// GLuint pAmbientColourLocation
		// GLuint pDiffuseIntensityLocation
		// GLuint pPositionLocation
		// GLuint pConstantLocation
		// GLuint pLinearLocation
		// GLuint pExponentLocation
		pPointLights[i].UseLight(
			aPointLightLocations[i].ambientIntensityLocation, 
			aPointLightLocations[i].ambientColorLocation, 
			aPointLightLocations[i].diffuseIntensityLocation, 
			aPointLightLocations[i].positionLocation, 
			aPointLightLocations[i].constantLocation, 
			aPointLightLocations[i].linearLocation, 
			aPointLightLocations[i].exponentLocation);
	}
	aPointLightCount = lightCount;
}

void Shader::SetSpotLights(vector<SpotLight> & pSpotLights) {
	unsigned int lightCount = pSpotLights.size() > MAX_SPOT_LIGHTS? MAX_SPOT_LIGHTS : pSpotLights.size();
	SetUniform1i(aSpotLightCountLocation, lightCount);

	for (uint i = 0; i<lightCount; i++) {
		// GLuint pAmbientIntensityLocation
		// GLuint pAmbientColourLocation
        // GLuint pDiffuseIntensityLocation
		// GLuint pPositionLocation
		// GLuint pDirectionLocation
        // GLuint pConstantLocation
		// GLuint pLinearLocation
		// GLuint pExponentLocation
        // GLuint pEdgeLocation
		pSpotLights[i].UseLight(
			aSpotLightLocations[i].baseLocation.ambientIntensityLocation, 
			aSpotLightLocations[i].baseLocation.ambientColorLocation,
			aSpotLightLocations[i].baseLocation.diffuseIntensityLocation,
			aSpotLightLocations[i].baseLocation.positionLocation, 
			aSpotLightLocations[i].directionLocation, 
			aSpotLightLocations[i].baseLocation.constantLocation,
			aSpotLightLocations[i].baseLocation.linearLocation,
			aSpotLightLocations[i].baseLocation.exponentLocation,
			aSpotLightLocations[i].edgeLocation);
	}
	aSpotLightCount = lightCount;
}

void Shader::SetTexture(GLuint pTextureUnit) {
	SetUniform1i(aTextureLocation, pTextureUnit);
}
void Shader::SetDirectionalShadowMap(GLuint pTextureUnit) {
	SetUniform1i(aDirectionalShadowMapLocation, pTextureUnit);
}
void Shader::SetDirectionalLightTransform(GLfloat * plTransform) {
	SetUniformMatrix4fv(aDirectionalLightTransformLocation, plTransform);
}
void Shader::SetDisplayShadows(bool pDisplayShadows) {
	aDisplayShadows = pDisplayShadows;
	SetUniform1i(aDisplayShadowsLocation, aDisplayShadows);
}

// 
vec3 Shader::GetColor() {
    return aColor;
}

// 
mat4 Shader::GetModelMatrix() {
    return aModelMatrix;
}

// 
mat4 Shader::GetViewMatrix() {
    return aViewMatrix;
}

// 
mat4 Shader::GetProjectionMatrix() {
    return aProjectionMatrix;
}

// 
GLboolean Shader::GetIsTextured() {
    return aIsTextured;
}

GLboolean Shader::GetDisplayShadows() {
	return aDisplayShadows;
}

// 
void Shader::UseShader() {
    glUseProgram(aShaderProgramId);
}

// 
void Shader::ClearShader() {
	if (aShaderProgramId != 0) {
		glDeleteProgram(aShaderProgramId);
		aShaderProgramId = 0;
	}
	aModelMatrixLocation = 0;
    aViewMatrixLocation = 0;
	aProjectionMatrixLocation = 0;
    aColorLocation = 0;

}

// 
Shader::~Shader() {
	ClearShader();
}

// PROTECTED
void Shader::CompileShader(const char* vertexCode, const char* fragmentCode) {
	aShaderProgramId = glCreateProgram();

	if (!aShaderProgramId)	{
		printf("Error creating shader program!\n");
		return;
	}

	AddShader(aShaderProgramId, vertexCode, GL_VERTEX_SHADER);
	AddShader(aShaderProgramId, fragmentCode, GL_FRAGMENT_SHADER);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glLinkProgram(aShaderProgramId);
	glGetProgramiv(aShaderProgramId, GL_LINK_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(aShaderProgramId, sizeof(eLog), NULL, eLog);
		printf("Error linking program: '%s'\n", eLog);
		return;
	}

	glValidateProgram(aShaderProgramId);
	glGetProgramiv(aShaderProgramId, GL_VALIDATE_STATUS, &result);
	if (!result)
	{
		glGetProgramInfoLog(aShaderProgramId, sizeof(eLog), NULL, eLog);
		printf("Error validating program: '%s'\n", eLog);
		return;
	}

	aProjectionMatrixLocation = GetUniformLocation("projection");
	aModelMatrixLocation = GetUniformLocation("model");
	aViewMatrixLocation = GetUniformLocation("view");
    aColorLocation = GetUniformLocation("fColor");
    aIsTexturedLocation = GetUniformLocation("isTextured");
    aDirectionalLightLocation.aAmbientColorLocation = GetUniformLocation("directionalLight.base.colour");
    aDirectionalLightLocation.aAmbientIntensityLocation = GetUniformLocation("directionalLight.base.ambientIntensity");
    aDirectionalLightLocation.aDiffuseIntensityLocation = GetUniformLocation("directionalLight.base.diffuseIntensity");
    aDirectionalLightLocation.aDirectionLocation = GetUniformLocation("directionalLight.direction");
	aSpecularIntensityLocation = GetUniformLocation("material.specularIntensity");
    aShininessLocation = GetUniformLocation("material.shininess");
    aEyePositionLocation = GetUniformLocation("eyePosition");
	aPointLightCountLocation = GetUniformLocation("pointLightCount");

	for (size_t i = 0; i<MAX_POINT_LIGHTS; i++) {
		char locBuff[100] = {'\0'};
		
		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].base.colour", i);
		aPointLightLocations[i].ambientColorLocation = GetUniformLocation(locBuff);

		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].base.ambientIntensity", i);
		aPointLightLocations[i].ambientIntensityLocation = GetUniformLocation(locBuff);

		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].base.diffuseIntensity", i);
		aPointLightLocations[i].diffuseIntensityLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].position", i);
		aPointLightLocations[i].positionLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].constant", i);
		aPointLightLocations[i].constantLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].linear", i);
		aPointLightLocations[i].linearLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "pointLights[%lu].exponent", i);
		aPointLightLocations[i].exponentLocation = GetUniformLocation(locBuff);

	}
	
	aSpotLightCountLocation = GetUniformLocation("spotLightCount");

	for (size_t i = 0; i<MAX_SPOT_LIGHTS; i++) {
		char locBuff[100] = {'\0'};
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.base.colour", i);
		aSpotLightLocations[i].baseLocation.ambientColorLocation = GetUniformLocation(locBuff);

		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.base.ambientIntensity", i);
		aSpotLightLocations[i].baseLocation.ambientIntensityLocation = GetUniformLocation(locBuff);

		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.base.diffuseIntensity", i);
		aSpotLightLocations[i].baseLocation.diffuseIntensityLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.position", i);
		aSpotLightLocations[i].baseLocation.positionLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.constant", i);
		aSpotLightLocations[i].baseLocation.constantLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.linear", i);
		aSpotLightLocations[i].baseLocation.linearLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].base.exponent", i);
		aSpotLightLocations[i].baseLocation.exponentLocation = GetUniformLocation(locBuff);

		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].direction", i);
		aSpotLightLocations[i].directionLocation = GetUniformLocation(locBuff);
		
		snprintf(locBuff, sizeof(locBuff), "spotLights[%lu].edge", i);
		aSpotLightLocations[i].edgeLocation = GetUniformLocation(locBuff);
	}

	aDirectionalLightTransformLocation  = GetUniformLocation("directionalLightSpaceTransform");
	aDirectionalShadowMapLocation = GetUniformLocation("directionalShadowMap");
    aTextureLocation = GetUniformLocation("fTextureSampler");
    aDisplayShadowsLocation = GetUniformLocation("displayShadows");
	
}

// 
void Shader::AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType) {
	GLuint theShader = glCreateShader(shaderType);

	const GLchar* theCode[1];
	theCode[0] = shaderCode;

	GLint codeLength[1];
	codeLength[0] = strlen(shaderCode);

	glShaderSource(theShader, 1, theCode, codeLength);
	glCompileShader(theShader);

	GLint result = 0;
	GLchar eLog[1024] = { 0 };

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result)
	{
		glGetShaderInfoLog(theShader, sizeof(eLog), NULL, eLog);
		printf("Error compiling the %d shader: '%s'\n", shaderType, eLog);
		return;
	}

	glAttachShader(theProgram, theShader);
}

// 
void Shader::SetUniformMatrix4fv(GLuint pLocation, GLfloat * pMatrix) {
    glUniformMatrix4fv(pLocation, 1, GL_FALSE, pMatrix);
}

// 
void Shader::SetUniform3fv(GLuint pLocation, vec3 & pVector) {
    glUniform3fv(pLocation, 1, &pVector[0]);
}

// 
void Shader::SetUniform1i(GLuint pLocation, GLint pInt) {
    glUniform1i(pLocation, pInt);
}