#include "ShadowMap.h"
ShadowMap::ShadowMap() {
    aFBO = 0;
    aShadowMapId = 0;
}

bool ShadowMap::Init(GLuint pWidth, GLuint pHeight) {
    aShadowWidth = pWidth;
    aShadowHeight = pHeight;

    glGenFramebuffers(1, &aFBO);
 
    glGenTextures(1, &aShadowMapId);
    glBindTexture(GL_TEXTURE_2D, aShadowMapId);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, aShadowWidth, aShadowHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    float borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
    
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, aFBO);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, aShadowMapId, 0);

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (status != GL_FRAMEBUFFER_COMPLETE) {
        printf("Framebuffer Error: %i\n", status);
        return false; 
    }
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    return true;
}

void ShadowMap::Write() {
    glBindFramebuffer(GL_FRAMEBUFFER, aFBO);
}

void ShadowMap::Read(GLenum pTextureUnit) {
    glActiveTexture(pTextureUnit);
    glBindTexture(GL_TEXTURE_2D, aShadowMapId);
}

ShadowMap::~ShadowMap() {
    if (aFBO)
        glDeleteFramebuffers(1, &aFBO);

    if (aShadowMapId)
        glDeleteTextures(1, &aShadowMapId);
}