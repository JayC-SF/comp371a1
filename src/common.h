#ifndef COMMON_H
#define COMMON_H
// COMP 371 Labs Framework
// ????
#include <cstdlib>
#pragma GCC optimize("O2")

#define GLEW_STATIC 1   // This allows linking with Static Library on Windows, without DLL
#include <GL/glew.h>    // Include GLEW - OpenGL Extension Wrangler

#include <GLFW/glfw3.h> // GLFW provides a cross-platform interface for creating a graphical context,
#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace glm;
using namespace std;

#define GLM_FORCE_RADIANS
#define WINDOW_WIDTH 1024
#define WINDOW_HEIGHT 768
#define ASPECT_RATIO ((float)WINDOW_WIDTH)/((float)WINDOW_HEIGHT)
// Defines the units of the program
#define U 1
#define X_DIR vec3(1.f, 0, 0)
#define Y_DIR vec3(0, 1.f, 0)
#define Z_DIR vec3(0, 0, 1.f)
#define GRID_SIZE 100
#define MODEL_SCALING_FACTOR 0.01
#define WASD_SPEED 2.*U
#define WORLD_ANGULAR_SPEED 5.f //degrees
#define PANNING_ANGULAR_SPEED 30.f //degrees
#define ZOOM_SPEED 1.f //degrees
#define NEAR_PLANE 0.01
#define FAR_PLANE 100.f
#define MAX_POINT_LIGHTS 3
#define MAX_SPOT_LIGHTS 3

#endif