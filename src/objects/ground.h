#ifndef GROUND_H
#define GROUND_H
#include <glm/glm.hpp>
#include "../buffer/include.h"
#include "../texture.h"
#include "atoms/cube.h"
#include "../shaders/include.h"
class Ground {
    public:
    Ground(GLuint pGroundSize, glm::vec3 pColor);
    void Draw(Shader * shader);
    // void ToggleTexture();
    protected:
    VAO aVAO;
    VBO aVBO;
    EBO aEBO;
    glm::vec3 aColor;
    // bool aIsTextured;
    glm::mat4 aGroundRotation;
    glm::mat4 aModelMatrix;

    // ground clay texture
    glm::mat4 aGroundMatrix;
};
#endif