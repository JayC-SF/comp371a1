#ifndef CUBE_H
#define CUBE_H
#include <glm/glm.hpp>
#include "atom.h"
#include <vector>
class Cube : public Atom{
    public:
    static Cube & GetInstance();
    void SetRepeatUVCoord(GLfloat pUVMaxCoords);
    void SetLineDrawingMode();
    void SetTriangleDrawingMode();
    void SetPointDrawingMode();
    ~Cube();
    Cube();

    protected:
    EBO aEBOLines;
    EBO aEBOPoints;
};
#endif