#include "cube.h"
#include "../../common.h"
#include "atom.h"

static vector<vec3> cubeVertices = {
    // front face 
    vec3(-U/2., -U/2., U/2.), vec3(U/2., -U/2., U/2.), vec3(U/2., U/2., U/2.),
    vec3(U/2., U/2., U/2.), vec3(-U/2., U/2., U/2.), vec3(-U/2., -U/2., U/2.),
    // back face
    vec3(U/2., -U/2., -U/2.), vec3(-U/2., -U/2., -U/2.), vec3(-U/2., U/2., -U/2.),
    vec3(-U/2., U/2., -U/2.), vec3(U/2., U/2., -U/2.), vec3(U/2., -U/2., -U/2.),
    // right face
    vec3(U/2., -U/2., U/2.), vec3(U/2., -U/2., -U/2.), vec3(U/2., U/2., -U/2.),
    vec3(U/2., U/2., -U/2.), vec3(U/2., U/2., U/2.), vec3(U/2., -U/2., U/2.),
    // left face
    vec3(-U/2., -U/2., -U/2.), vec3(-U/2., -U/2., U/2.), vec3(-U/2., U/2., U/2.),
    vec3(-U/2., U/2., U/2.), vec3(-U/2., U/2., -U/2.), vec3(-U/2., -U/2., -U/2.),
    // top face
    vec3(-U/2., U/2., U/2.), vec3(U/2., U/2., U/2.), vec3(U/2., U/2., -U/2.),
    vec3(U/2., U/2., -U/2.), vec3(-U/2., U/2., -U/2.), vec3(-U/2., U/2., U/2.),
    // bottom face
    vec3(-U/2., -U/2., -U/2.), vec3(U/2., -U/2., -U/2.), vec3(U/2., -U/2., U/2.),
    vec3(U/2., -U/2., U/2.), vec3(-U/2., -U/2., U/2.), vec3(-U/2., -U/2., -U/2.),
    };

static  vector<GLuint> cubeLineIndices = {
    // front face
    0, 1,
    1, 2,
    3, 4,
    4, 5,

    // back face
    6, 7, 
    7, 8,
    9, 10,
    10, 11,

    // side lines
    0, 7,
    1, 6, 
    3, 10,
    4, 9
};

// to be initialized once
static vector<GLuint> cubePointAndTriangleIndices;

vector<vec3> normals {
    // front face
    Z_DIR, Z_DIR, Z_DIR, Z_DIR, Z_DIR, Z_DIR,
    // back face
    -Z_DIR, -Z_DIR, -Z_DIR, -Z_DIR, -Z_DIR, -Z_DIR,
    // right face
    X_DIR, X_DIR, X_DIR, X_DIR, X_DIR, X_DIR,
    // left face
    -X_DIR, -X_DIR, -X_DIR, -X_DIR, -X_DIR, -X_DIR,
    // top face
    Y_DIR, Y_DIR, Y_DIR, Y_DIR, Y_DIR, Y_DIR,
    // bottom face
    -Y_DIR, -Y_DIR, -Y_DIR, -Y_DIR, -Y_DIR, -Y_DIR,
};

static bool areStaticDataInit = false;

// create a vector of 6 faces with 6 vertex per face
vector<vec2> UV(6*6); //{
//     // front face 
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f), 
//     // back face
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f), 
//     // right face
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f), 
//     // left face
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f), 
//     // top face
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f), 
//     // bottom face
//     vec2(0.f, 0.f), vec2(1.f, 0.f), vec2(1.f, 1.f),
//     vec2(1.f, 1.f), vec2(0.f, 1.f), vec2(0.f, 0.f),
// };

static void setCubeUV(GLfloat pUVMaxCoords) {
    for (int i = 0; i<6*6; i+=6) {
        UV[i] = vec2(0.f, 0.f);
        UV[i + 1] = vec2(pUVMaxCoords, 0.f);
        UV[i + 2] = vec2(pUVMaxCoords, pUVMaxCoords);
        UV[i + 3] = vec2(pUVMaxCoords, pUVMaxCoords);
        UV[i + 4] = vec2(0.f, pUVMaxCoords);
        UV[i + 5] = vec2(0.f, 0.f);
    }
}

static void initStaticData() { 
    if (areStaticDataInit)
        return;
    for (int i = 0; i<cubeVertices.size(); i++)
        cubePointAndTriangleIndices.push_back(i);
    setCubeUV(1.f);
    areStaticDataInit = true;
}

// Constructor function of the Cube object.
// initialize the aCurrentEBO reference to default EBO.
Cube::Cube() : Atom(){
    // init missing indices
    initStaticData();
    // UV locations for texture mapping.
    
    aVAO.Bind();
    // init and bind vertex vbo
    aVBOVertex.Init(cubeVertices.size()*sizeof(vec3), &cubeVertices[0][0], GL_STATIC_DRAW);
    aVAO.VAttribPointer(aVBOVertex, 0, 3, GL_FLOAT, sizeof(vec3), NULL);

    // init and bind uv vbo
    aVBOUV.Init(UV.size()*sizeof(vec2), &UV[0][0], GL_STATIC_DRAW);
    aVAO.VAttribPointer(aVBOUV, 1, 2, GL_FLOAT, sizeof(vec2), NULL);

    // init and bind normal vbo
    aVBONormals.Init(normals.size()*sizeof(vec3), &normals[0][0], GL_STATIC_DRAW);
    aVAO.VAttribPointer(aVBOVertex, 2, 3, GL_FLOAT, sizeof(vec3), NULL);
    
    // default EBO.
    aEBO.Init(cubePointAndTriangleIndices.size(), &cubePointAndTriangleIndices[0], GL_STATIC_DRAW, GL_TRIANGLES);
    // lines EBO
    aEBOLines.Init(cubeLineIndices.size(), &cubeLineIndices[0], GL_STATIC_DRAW, GL_LINES);
    // points EBO
    aEBOPoints.Init(cubePointAndTriangleIndices.size(), &cubePointAndTriangleIndices[0], GL_STATIC_DRAW, GL_POINTS);
    
    aVAO.Unbind();
}

void Cube::SetLineDrawingMode() {
    aCurrentEBO = &aEBOLines;
}
void Cube::SetTriangleDrawingMode() {
    aCurrentEBO = &aEBO;
}
void Cube::SetPointDrawingMode() {
    aCurrentEBO = &aEBOPoints;
}

void Cube::SetRepeatUVCoord(GLfloat pUVMaxCoords) {
    setCubeUV(pUVMaxCoords);
    aVAO.Bind();
    aVBOUV.SubBufferData(UV.size()*sizeof(vec2), &UV[0][0]);
}

Cube & Cube::GetInstance() {
    static Cube instance = Cube();
    return instance;
}

Cube::~Cube() {}