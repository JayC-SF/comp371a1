#include "racket.h"
#include "../common.h"
#include "atoms/cube.h"
#include "../shaders/include.h"
#include "../texture.h"
// 
Racket::Racket() :
// define default initialized values;
// aShaderProgramId(pShaderProgramId),
// aModelMatrixLocation(pModelMatrixLocation), 
// aColorLocation(pColorLocation),
aTranslation(mat4(1.f)),
aRotation(mat4(1.f)), 
aBaseGroupMatrix(mat4(1.f)),
aTotalModelScale(1.f),
aColor(vec3(1.f, 0.f, 0.f)), 
aIsUpdated(false)
{
    vec3 racketBlockDims(0.15f*U, 1.f*U, 0.15f*U);
    mat4 cubeOnGround = translate(mat4(1.f), 0.5f * Y_DIR);

    // create a new block and add 0.3f to the height of the block to 
    vec3 racketHandleDims = racketBlockDims + vec3(0.f, .1f, 0.f);
    aRacketHandle = scale(mat4(1.f), racketHandleDims) * cubeOnGround;

    // scale the stick to be sitting on xz plane and length vertically.
    // create a bottom rim with racket handle dimensions. 
    // bottom face facing the ground
    // and x and z centered at origin. 
    GLfloat rimThickness = racketHandleDims.z*0.5;
    vec3 racketTopBotRimDims = vec3(racketHandleDims.y*0.9, rimThickness, rimThickness);
    mat4 shapeRacketBottomRim = scale(mat4(1.f), racketTopBotRimDims);
    mat4 transformRacketBottomRimGroup = translate(mat4(1.f), racketHandleDims.y * U * Y_DIR);
    aRacketBottomRim =  transformRacketBottomRimGroup * shapeRacketBottomRim * cubeOnGround;

    vec3 sideRimDims = vec3(rimThickness, racketHandleDims.y * 1.5f, rimThickness);
    mat4 scaledSideRim = scale(mat4(1.f), sideRimDims) * cubeOnGround;
    vec3 posXSideRimTranslation = (racketTopBotRimDims.x + sideRimDims.x)/2.f *U*X_DIR;
    // negative x translation.
    mat4 racketLeftRimGroup = translate(transformRacketBottomRimGroup, -posXSideRimTranslation);
    aRacketLeftRim = racketLeftRimGroup * scaledSideRim;
    
    // right racket rim.
    // positive x translation
    aRacketRightRim = translate(transformRacketBottomRimGroup, posXSideRimTranslation) * scaledSideRim;
    aRacketTopRim = translate(mat4(1.f), (sideRimDims.y - racketTopBotRimDims.y)*U*Y_DIR) * aRacketBottomRim;

    GLfloat meshHeight = sideRimDims.y - 2 * racketTopBotRimDims.y;
    GLfloat meshWidth = racketTopBotRimDims.x;
    // make the spacing between strings 10% of the mesh width
    GLfloat spacingBetweenStrings = 0.10 * meshWidth;
    // make the mesh strings width to be half of the spacing between them.
    GLfloat stringWidth = 0.08 * spacingBetweenStrings;
    vec3 verticalStringDims(stringWidth, meshHeight, stringWidth);
    vec3 horizontalStringDims(meshWidth, stringWidth, stringWidth);
    
    // don't create first and last one on the horizontal axis
    vec3 stringPos(
        (-meshWidth/2 + spacingBetweenStrings)*U, // x
        racketTopBotRimDims.y*U,                  // y
        0.f);                                     // z;

    mat4 stringMatrix = scale(mat4(1.f), verticalStringDims) * cubeOnGround;
    for (; stringPos.x < meshWidth/2.f; stringPos.x += spacingBetweenStrings + verticalStringDims.x) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.f), stringPos) *
            stringMatrix;
        aMeshMatrices.push_back(m);
    }
    stringPos.y = racketTopBotRimDims.y + spacingBetweenStrings;
    stringPos.x = 0;
    stringMatrix = scale(mat4(1.f), horizontalStringDims) * cubeOnGround;
    for (; stringPos.y < meshHeight + racketTopBotRimDims.y; stringPos.y += spacingBetweenStrings + horizontalStringDims.y) {
        mat4 m = 
            transformRacketBottomRimGroup *
            translate(mat4(1.f), stringPos) *
            stringMatrix;
        aMeshMatrices.push_back(m);
    }
}

// 
void Racket::Draw(Shader * shader) {
    Update();
    Cube cube = Cube::GetInstance();
    cube.BindAttributes();
    Texture & glossyTexture = Texture::GetGlossyTexture();
    glossyTexture.UseTexture();

    // glUniform3fv(aColorLocation, 1, &(vec3(1.0f, 0.f, 0.f)[0]));
    shader->SetColor(vec3(1.0f, 0.f, 0.f));
    // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aRacketHandle)[0][0]));
    shader->SetModelMatrix(aWristMatrix * aRacketHandle);
    cube.SetRepeatUVCoord(1.f);
    cube.Draw();
    
    // draw single diagonal on stick
    // glUniform3fv(aColorLocation, 1, &(vec3(1.0f, 1.0f, 1.0f)[0]));
    shader->SetColor(vec3(1.f));
    // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aRacketBottomRim)[0][0]));
    shader->SetModelMatrix(aWristMatrix * aRacketBottomRim);

    cube.Draw();
    
    
    // draw left rim
    // glUniform3fv(aColorLocation, 1, &(vec3(1.0f, 0.f, 0.f)[0]));
    shader->SetColor(vec3(1.f, 0.f, 0.f));
    // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aRacketLeftRim)[0][0]));
    shader->SetModelMatrix(aWristMatrix * aRacketLeftRim);
    cube.Draw();
    
    // draw right rim
    // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aRacketRightRim)[0][0]));
    shader->SetModelMatrix(aWristMatrix * aRacketRightRim);
    cube.Draw();
    
    // draw top rim
    // glUniform3fv(aColorLocation, 1, &(vec3(1.f, 1.f, 1.f)[0]));
    // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aRacketTopRim)[0][0]));
    shader->SetColor(vec3(1.f));
    shader->SetModelMatrix(aWristMatrix * aRacketTopRim);
    cube.Draw();
    bool isTextured = shader->GetIsTextured();
    shader->SetIsTextured(false);
    shader->SetColor(vec3(0.f, 1.f, 0.f));
    for (int i = 0; i<aMeshMatrices.size(); i++) {
        // glUniform3fv(aColorLocation, 1, &(vec3(0.f, 1.f, 0.f)[0]));
        // glUniformMatrix4fv(aModelMatrixLocation, 1, GL_FALSE, &((aWristMatrix * aMeshMatrices[i])[0][0]));
        shader->SetModelMatrix(aWristMatrix * aMeshMatrices[i]);
        cube.Draw();
    }
    shader->SetIsTextured(isTextured);

    cube.UnbindAttributes();
}

// 
void Racket::Translate(vec3 pTranslation) {
    aTranslation = translate(mat4(1.f), pTranslation) * aTranslation;
    aIsUpdated = false;
}

// 
void Racket::Rotate(GLfloat pRads, vec3 pAxis) {
    aRotation = rotate(mat4(1.f), pRads, pAxis) * aRotation;
    aIsUpdated = false;
}

// 
void Racket::Scale(GLfloat pScale) {
    aTotalModelScale += pScale;
    aIsUpdated = false;
}

// 
void Racket::SetBaseGroupMatrix(mat4 pBaseGroupMatrix) {
    aBaseGroupMatrix = pBaseGroupMatrix;
    aIsUpdated = false;
}

// 
void Racket::SetColor(vec3 pColor) {
    aColor = pColor;
}

// 
void Racket::Update() {
    if(aIsUpdated)
        return;
    aWristMatrix = aBaseGroupMatrix * aTranslation * aRotation * scale(mat4(1.f), aTotalModelScale*vec3(1.f));
    aIsUpdated = true;
}