#ifndef SPOT_LIGHT_H
#define SPOT_LIGHT_H
#include "pointLight.h"

class SpotLight : PointLight{
    public:
    SpotLight();

    SpotLight(
        vec3 pColor, 
        GLfloat pIntensity, 
        GLfloat pDiffuseIntensity, 
        vec3 pPosition, 
        vec3 pDirection, 
        GLfloat pConstant, 
        GLfloat pLinear,
        GLfloat  pExponent, 
        GLfloat pEdge);

    void UseLight(
        GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
        GLuint pDiffuseIntensityLocation, GLuint pPositionLocation, GLuint directionLocation,
        GLuint pConstantLocation, GLuint pLinearLocation,GLuint pExponentLocation,
        GLuint pEdgeLocation);

    protected:
    vec3 aDirection;

    GLfloat aEdge;    
    GLfloat aProcEdge;

};
#endif