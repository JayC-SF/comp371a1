#include "spotLight.h"
#include "pointLight.h"

SpotLight::SpotLight() : PointLight() {
    aDirection = vec3(0.f, -1.f, 0.f);
    aEdge = 0.f;

}

SpotLight::SpotLight(
        vec3 pColor, 
        GLfloat pIntensity, 
        GLfloat pDiffuseIntensity, 
        vec3 pPosition, 
        vec3 pDirection, 
        GLfloat pConstant, 
        GLfloat pLinear,
        GLfloat pExponent,
        GLfloat pEdge) : PointLight(pColor, pIntensity, pDiffuseIntensity, pPosition, pConstant, pLinear, pExponent) {
            aDirection = normalize(pDirection);
            aEdge = pEdge;
            aProcEdge = cosf(aEdge);
}

void SpotLight::UseLight(
        GLuint pAmbientIntensityLocation, GLuint pAmbientColourLocation, 
        GLuint pDiffuseIntensityLocation, GLuint pPositionLocation, GLuint pDirectionLocation,
        GLuint pConstantLocation, GLuint pLinearLocation,GLuint pExponentLocation,
        GLuint pEdgeLocation) {
    
    PointLight::UseLight(pAmbientIntensityLocation, 
        pAmbientColourLocation,
        pDiffuseIntensityLocation,
        pPositionLocation,
        pConstantLocation,
        pLinearLocation,
        pExponentLocation);
    
    glUniform3fv(pDirectionLocation, 1, &aDirection[0]);
    glUniform1f(pEdgeLocation, aProcEdge);
}