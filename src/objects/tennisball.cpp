#include "tennisball.h"
#include "atoms/sphere.h"
#include "../texture.h"
#include "../shaders/shader.h"

TennisBall::TennisBall() :
aModelMatrix(mat4(1.f)), 
aScale(mat4(1.f)), 
aRotation(mat4(1.f)), 
aTranslation(mat4(1.f)), 
aRadius(1.f), 
aIsUpdated(false)
{}

TennisBall::TennisBall(GLfloat pRadius) :
aModelMatrix(mat4(1.f)), 
aScale(scale(mat4(1.f), vec3(pRadius))), 
aRotation(mat4(1.f)), 
aTranslation(mat4(1.f)), 
aRadius(pRadius), 
aIsUpdated(false)
{}


void TennisBall::Draw(Shader * shader) {
    Update();
    Sphere & sphere = Sphere::GetInstance();
    Texture & tennisTexture = Texture::GetTennisBallTexture();
    shader->UseShader();
    sphere.BindAttributes();
    tennisTexture.UseTexture();
    shader->SetModelMatrix(aModelMatrix);
    sphere.Draw();
    sphere.UnbindAttributes();
    
}

void TennisBall::SetRadius(GLfloat pRadius) {
    aRadius = pRadius;
    aScale = scale(mat4(1.f), vec3(aRadius));
}

void TennisBall::Translate(vec3 pTranslate) {
    aTranslation = translate(mat4(1.f), pTranslate) * aTranslation;
    aIsUpdated = false;
}
void TennisBall::aRotate(GLfloat aRads, vec3 pAxis) {
    aRotation = rotate(mat4(1.f), aRads, pAxis) * aRotation;
    aIsUpdated = false;
}

void TennisBall::Update() {
    if(aIsUpdated)
        return;
    aModelMatrix = aTranslation * aRotation * aScale;
    aIsUpdated = true;
}
