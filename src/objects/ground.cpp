#include "ground.h"
#include <vector>
#include "../common.h"
#include "atoms/cube.h"
#include "../shaders/include.h"


Ground::Ground(GLuint pGroundSize, vec3 pColor)
{
    vector<vec3> vertices;
    vector<GLuint> indices;
    GLdouble halfGround = pGroundSize/2.0f * U;
    // start with corner.
    for (int i = 0; i<=pGroundSize; i++) {
        // set a pair of vertices to form a line, placing one on positive x and other on negative x.
        // Building lines from the negative z axis to the positive axis.
        vertices.push_back(vec3(-halfGround, 0.f, -halfGround + i*U));
        vertices.push_back(vec3(halfGround, 0.f, -halfGround + i*U));
    }
    // start at 1 because pair of vertices are already setup from previous for loop.
    for (int i = 0; i<=pGroundSize; i++) {
        // set a pair of vertices to form a line, placing one on positive x and other on negative x.
        // Building lines from the negative z axis to the positive axis.
        vertices.push_back(vec3(-halfGround + i*U, 0.f, -halfGround));
        vertices.push_back(vec3(-halfGround + i*U, 0.f, halfGround));
    }
    for (int i = 0; i < vertices.size(); i++) {
        indices.push_back(i);
    }

    aVAO = VAO();
    aVAO.Bind();
    aVBO.Init(vertices.size()*sizeof(vec3), &vertices[0][0], GL_STATIC_DRAW);
    aVAO.VAttribPointer(aVBO, 0, 3, GL_FLOAT, sizeof(vec3), NULL);
    aEBO.Init(indices.size(), &indices[0], GL_STATIC_DRAW, GL_LINES);

    aColor = pColor;
    aGroundRotation = mat4(1.0);
    aModelMatrix = mat4(1.0);
    // aIsTextured = false;
    
    // ground for the cube.
    // translate the cube right below the xz plane
    // scale the cube by 100 on x and z axis
    // 
    aGroundMatrix = scale(mat4(1.0), vec3(100.f, 1.f, 100.f)) * translate(mat4(1.f), -1.f*Y_DIR);
    // aClayTexture = Texture("../assets/textures/clay-tennis-court.png");
    // aClayTexture.LoadTexture();
}

void Ground::Draw(Shader * shader) {
    Cube cube = Cube::GetInstance();
    shader->UseShader();
    // bind info in shader.
    shader->SetModelMatrix(aGroundMatrix);
    Texture & clayTexture = Texture::GetClayTexture();
    clayTexture.UseTexture();

    shader->SetColor(vec3(1.f));
    cube.BindAttributes();
    cube.Draw();
    
    shader->SetColor(vec3(1.f));
    shader->SetModelMatrix(aModelMatrix);
    aVAO.Bind();
    aEBO.Bind();
    aEBO.Draw();
    aEBO.Unbind();
    aVAO.Unbind();
    // unbind and cleanup
    cube.UnbindAttributes();
}

// void Ground::ToggleTexture() {
//     aIsTextured = !aIsTextured;
// }