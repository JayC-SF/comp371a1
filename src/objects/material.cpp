#include "material.h"

Material::Material::Material() {
    aSpecularIntensity = 0.f;
    aShininess = 0.f;
}
Material::Material(GLfloat pSpecularIntensity, GLfloat pShininess){
    aSpecularIntensity = pSpecularIntensity;
    aShininess = pShininess;
}

void Material::UseMaterial(GLuint pSpecularLocation, GLuint pShininessLocation) {
    glUniform1f(pSpecularLocation, aSpecularIntensity);
    glUniform1f(pShininessLocation, aShininess);
}
Material::~Material(){
    
} 