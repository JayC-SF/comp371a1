#include "common.h"
#include "buffer/include.h"
#include "objects/directionalLight.h"
#include "objects/include.h"
#include "objects/pointLight.h"
#include "objects/spotLight.h"
#include "objects/tennisball.h"
#include "shaders/shader.h"
#include "texture.h"

#include <filesystem>
#include <iostream>
#include <vector>
#include <random>

typedef struct {
    GLuint key;
    GLuint state;
} last_state;

// create 1X1 cube.

GLfloat randomFloat(GLfloat min, GLfloat max) {
    
    double randomNum = ((double)rand())/RAND_MAX;
    randomNum *= (max - min);
    randomNum += min;
    return randomNum;
}

void renderScene(Shader * shader, Material * dullMaterial, Material * shinyMaterial, Ground * ground, Arm * arm, 
    DirectionalLight * mainLight,
    vector<SpotLight> * spotLights,
    vector<PointLight> * pointLights,
    vec3 eyePosition, 
    TennisBall * tennisBall) {
    // LIGHTING
        shader->UseShader();
        shader->SetDirectionalLight(mainLight);
        shader->SetPointLights(*pointLights);
        shader->SetSpotLights(*spotLights);
        shader->SetEyePosition(eyePosition);
        
        // Draw g1 * g2 * g3 mvp
        dullMaterial->UseMaterial(shader->GetSpecularIntensityLocation(), shader->GetShininessLocation());
        ground->Draw(shader);
        shinyMaterial->UseMaterial(shader->GetSpecularIntensityLocation(), shader->GetShininessLocation());
        arm->Draw(shader);
        dullMaterial->UseMaterial(shader->GetSpecularIntensityLocation(), shader->GetShininessLocation());
        
        // Draw the tennis ball
        tennisBall->Draw(shader);
}

void directionalShadowMapPass(Shader * directionalShadowShader, Material *dullMaterial, Material *shinyMaterial, Ground *ground, Arm *arm, DirectionalLight *mainLight, vector<SpotLight> *spotLights, vector<PointLight> *pointLights, vec3 eyePosition, TennisBall *tennisBall
) {
    directionalShadowShader->UseShader();

    glViewport(0, 0, mainLight->GetShadowMap()->GetShadowWidth(), mainLight->GetShadowMap()->GetShadowHeight());
    
    mainLight->GetShadowMap()->Write();

    glClear(GL_DEPTH_BUFFER_BIT);

    // uniformModel = directionalShadowShader->GetModelLocation();
    mat4 directionalLightTransform = mainLight->CalculateLightTransform();
    
    directionalShadowShader->SetDirectionalLightTransform(&directionalLightTransform[0][0]);
    
    renderScene(directionalShadowShader, dullMaterial, shinyMaterial, ground, arm, mainLight, spotLights, pointLights, eyePosition, tennisBall);

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void renderPass(Shader * shader, Material *dullMaterial, Material *shinyMaterial, Ground *ground, Arm *arm, DirectionalLight *mainLight, vector<SpotLight> *spotLights, vector<PointLight> *pointLights, vec3 eyePosition, TennisBall *tennisBall) {
    shader->UseShader();
}
int main(int argc,  char * argv[]) {
    
    // Init the glfw library and set various configurations.
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create a glfw window with specified size
    // glfwCreateWindow(width, size, title, monitor, share)
    GLFWwindow * window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Tennis Racket Assignment 1", NULL, NULL);
    // If cannot create window we abort the program.
    if (!window) {
        std::cerr << "Failed to open GLFW Window." << std::endl;
        exit(EXIT_FAILURE);
    }
    // Set context current context to this window.
    glfwMakeContextCurrent(window);
    // disable the mouse
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    // Initialize GLEW
    // Set experimental features needed for core profile
    glewExperimental = true;
    // init glew and check if it was properly init.
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to init GLEW" << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    // GLuint VAOTriangleCube, VBOTriangleCube, EBOTriangleCube, EBOTriangleCubeLength;
    // renderTriangleCube(VAOTriangleCube, VBOTriangleCube, EBOTriangleCube, EBOTriangleCubeLength);

    glClearColor(37.f/255., 30.f/255., 62.f/255., 1);
    // glShaderSource

    // Set the viewport to avoid the jittering in the beginning.
    GLint frameBufferWidth, frameBufferHeight;
    glfwGetFramebufferSize(window, &frameBufferWidth, &frameBufferHeight);
    glViewport(0, 0, frameBufferWidth, frameBufferHeight);

    Cube & cube = Cube::GetInstance();
    cube.BindAttributes();
    Shader shader = Shader("../assets/shaders/vertexShader.glsl", "../assets/shaders/fragmentShader.glsl");
    Shader directionalShadowShader = Shader("../assets/shaders/vertexDirectionalShadowMap.glsl", "../assets/shaders/fragmentDirectionalShadowMap.glsl");
    cube.UnbindAttributes();
    shader.UseShader();
    mat4 defaultModelMatrix(1.0);

    mat4 currentWorldRotation(1.0);
    GLfloat cameraHorizontalAngle = 90;
    GLfloat cameraVerticalAngle = 0;
    GLfloat theta = radians(cameraHorizontalAngle);
    GLfloat phi = radians(cameraVerticalAngle);
    vec3 cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
    vec3 cameraPosition = vec3(0.f*U, 4.0f*U, 25.f*U);
    vec3 eyePosition = vec3(0.f*U, 4.0f*U, 25.f*U);
    
    vec3 cameraUp = vec3(0.f*U, 1.0f*U, 0.f*U);
    mat4 initialWorldPanning = lookAt(cameraPosition, cameraPosition+cameraLookAt, vec3(0.f*U, 1.0f*U, 0.f*U));
    mat4 currentWorldPanning = initialWorldPanning;
    mat4 viewMatrix = currentWorldRotation * currentWorldPanning;
    shader.SetViewMatrix(viewMatrix);

    // set projection matrix to perspective view.
    GLfloat fovy = radians(60.f);
    mat4 perspectiveMatrix = perspective(fovy, ASPECT_RATIO, 0.01f*U, 1000.0f*U);
    shader.SetProjectionMatrix(perspectiveMatrix);

    // isTextured colorLocation
    shader.SetIsTextured(false);

    // 1 unit a second.
    GLfloat lastFrame = glfwGetTime();
    // keep the last mouse state at all times.
    GLdouble lastMousePosX, lastMousePosY;
    glfwGetCursorPos(window, &lastMousePosX, &lastMousePosY);
    // Enables the Depth Buffer
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);

    GLuint spaceLastState = glfwGetKey(window, GLFW_KEY_SPACE);
    GLuint X_lastState = glfwGetKey(window, GLFW_KEY_X);
    GLuint B_lastState = glfwGetKey(window, GLFW_KEY_B);

    Ground ground = Ground(GRID_SIZE, vec3(1.f, 1.f, 0));
    Sphere sphere = Sphere::GetInstance();
    Arm arm = Arm();
    TennisBall tennisBall(.5f);
    tennisBall.Translate(4.f * Y_DIR);

    DirectionalLight mainLight = DirectionalLight(2048*2, 2048*2, vec3(1.f, 1.f, 1.f), 0.0f, 
        vec3(.0f, -5.f, -10.f), 0.f);

    vector<PointLight> pointLights;
    // PointLight(vec3 pColor, GLfloat pIntensity, GLfloat pDiffuseIntensity, 
    //     vec3 pPosition, GLfloat pConstant, GLfloat pLinear,GLfloat  pExponent);
    pointLights.push_back(PointLight(vec3(1.f, 1.f, 1.f), 0.8f, 1.f, vec3(0.f, 30.f, 0.f), 1.f, 0.01f, 0.001f));

    vector<SpotLight> spotLights;

    // vec3 pColor
    // GLfloat pIntensity
    // GLfloat pDiffuseIntensity
    // vec3 pPosition
    // vec3 pDirection
    // GLfloat pConstant
    // GLfloat pLinear
    // GLfloat pExponent
    // GLfloat pEdge
    // spotLights.push_back(SpotLight(
    //     vec3(1.f, 1.f, 0.f),
    //     0.2f,
    //     1.f, 
    //     vec3(3.f, 1.f, 0.f), 
    //     -Y_DIR, 
    //     0.3f, 0.1f, 0.05f, 
    //     radians(90.f)
    // ));
    
    // 32 is quite the right number for a regular shiny object
    Material shinyMaterial(1.0f, 32);

    Material dullMaterial(0.3f, 4);
    shader.SetDisplayShadows(true);
    while(!glfwWindowShouldClose(window)) {
        GLfloat dt = glfwGetTime() - lastFrame;
        lastFrame += dt;
        GLdouble mousePosX, mousePosY;
        glfwGetCursorPos(window, &mousePosX, &mousePosY);
        // set the dx and dy for the mouse.
        double dx = mousePosX - lastMousePosX, dy = mousePosY - lastMousePosY;
        lastMousePosX = mousePosX;
        lastMousePosY = mousePosY;

        directionalShadowMapPass(&directionalShadowShader, &dullMaterial, &shinyMaterial, &ground, &arm, &mainLight, &spotLights, &pointLights, eyePosition, &tennisBall);

        // clear current buffer
        glViewport(0, 0, frameBufferWidth, frameBufferHeight);
        glClearColor(0, 0, 0, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader.UseShader();
        
        
        mainLight.GetShadowMap()->Read(GL_TEXTURE1);
	    
        shader.SetTexture(0);
	    shader.SetDirectionalShadowMap(1);
        
        mat4 mainLightTransform = mainLight.CalculateLightTransform();
        shader.SetDirectionalLightTransform(&mainLightTransform[0][0]);
        
        renderScene(&shader, &dullMaterial, &shinyMaterial, &ground, &arm, &mainLight, &spotLights, &pointLights, eyePosition, &tennisBall);
        
        // swap the buffer to the one already written, and place the current one as a canvas
        glfwSwapBuffers(window);

        glfwPollEvents();
        
        // Upon pressing escape, stop window, and terminate program later.
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);

        
        bool shiftPressed = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS; 
        shiftPressed = shiftPressed || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS;
        
        if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS) {
            cube.SetTriangleDrawingMode();
        }

        if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS) {
            cube.SetPointDrawingMode();
        }

        if (glfwGetKey(window, GLFW_KEY_L) == GLFW_PRESS) {
            cube.SetLineDrawingMode();
        }

        if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(-distanceTravel*U * X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(-radians<GLfloat>(5.f), -Z_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(-radians<GLfloat>(5.f), -Z_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
            // unit translation to the left
            arm.Rotate(-radians<GLfloat>(5), -Z_DIR);
        }

        if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(distanceTravel*U * X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(radians<GLfloat>(5.f), -Z_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(radians<GLfloat>(5.f), -Z_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
            // unit translation to the left
            arm.Rotate(radians<GLfloat>(5), -Z_DIR);
        }

        if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(distanceTravel* U * Y_DIR);
        } 
        else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(radians<GLfloat>(-5.f), X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(radians<GLfloat>(-5.f), X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
            arm.Rotate(radians<GLfloat>(-5.f), X_DIR);
        }
        if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(-distanceTravel * U * Y_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(radians<GLfloat>(5.f), X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(radians<GLfloat>(5.f), X_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
            arm.Rotate(radians<GLfloat>(5.f), X_DIR);
        }

        if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(radians<GLfloat>(-5.f), Y_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(radians<GLfloat>(-5.f), Y_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
            arm.Rotate(radians<GLfloat>(-5.f), Y_DIR);
        }
        if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_1)) {
            arm.RotateElbow(radians<GLfloat>(5.f), Y_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS && glfwGetKey(window, GLFW_KEY_2)) {
            arm.RotateWrist(radians<GLfloat>(5.f), Y_DIR);
        }
        else if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
            arm.Rotate(radians<GLfloat>(5.f), Y_DIR);
        }

        if (glfwGetKey(window, GLFW_KEY_Z) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(-distanceTravel * U * Z_DIR);
        }
        if (glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS && shiftPressed) {
            // unit translation to the left
            GLfloat distanceTravel = WASD_SPEED * dt;
            arm.Translate(distanceTravel * U * Z_DIR);
        }
        else if (X_lastState == GLFW_RELEASE && glfwGetKey(window, GLFW_KEY_X) == GLFW_PRESS) {
            shader.SetIsTextured(!shader.GetIsTextured());
        }

        if (glfwGetKey(window, GLFW_KEY_U) == GLFW_PRESS && shiftPressed) {
            arm.Scale(MODEL_SCALING_FACTOR);
        }
        if (glfwGetKey(window, GLFW_KEY_J) == GLFW_PRESS && shiftPressed) {
            arm.Scale(-MODEL_SCALING_FACTOR);
        }

        if (spaceLastState == GLFW_RELEASE && glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
            // random location on the grid.
            GLfloat halfGrid = GRID_SIZE*U/2.f;
            vec3 randomLocation(randomFloat(-halfGrid, halfGrid), 0, randomFloat(-halfGrid, halfGrid));
            arm.ResetTranslation();
            arm.Translate(randomLocation);
        }
        // 
        if (glfwGetKey(window, GLFW_KEY_HOME) == GLFW_PRESS) {
            // viewMatrix = initialWorldPanning;
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
            eyePosition = vec3(0.f*U, 4.0f*U, 25.f*U);
            shader.SetViewMatrix(initialWorldPanning);
            arm.Reset();
        }
        // world rotation about the axis
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
            currentWorldRotation = rotate(mat4(1.0), radians(WORLD_ANGULAR_SPEED), Y_DIR);
            eyePosition = rotate(mat4(1.0), radians(-WORLD_ANGULAR_SPEED), Y_DIR) * vec4(eyePosition, 1.f);
            mat4 viewMatrix = shader.GetViewMatrix();
            viewMatrix = currentWorldPanning * currentWorldRotation * inverse(currentWorldPanning) * viewMatrix;
            shader.SetViewMatrix(viewMatrix);
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);

        }
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {
            currentWorldRotation = rotate(mat4(1.0), radians(-WORLD_ANGULAR_SPEED), Y_DIR);
            eyePosition = rotate(mat4(1.0), radians(WORLD_ANGULAR_SPEED), Y_DIR) * vec4(eyePosition, 1.f);
            mat4 viewMatrix = shader.GetViewMatrix();
            viewMatrix = currentWorldPanning * currentWorldRotation * inverse(currentWorldPanning) * viewMatrix;
            shader.SetViewMatrix(viewMatrix);
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
        }
        if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
            currentWorldRotation = rotate(mat4(1.0), radians(WORLD_ANGULAR_SPEED), X_DIR);
            eyePosition = rotate(mat4(1.0), radians(-WORLD_ANGULAR_SPEED), X_DIR) * vec4(eyePosition, 1.f);
            mat4 viewMatrix = shader.GetViewMatrix();
            viewMatrix = currentWorldPanning * currentWorldRotation * inverse(currentWorldPanning) * viewMatrix;
            shader.SetViewMatrix(viewMatrix);

            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
        }
        if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
            currentWorldRotation = rotate(mat4(1.0), radians(-WORLD_ANGULAR_SPEED), X_DIR);
            eyePosition = rotate(mat4(1.0), radians(WORLD_ANGULAR_SPEED), X_DIR) * vec4(eyePosition, 1.f);
            mat4 viewMatrix = shader.GetViewMatrix();
            viewMatrix = currentWorldPanning * currentWorldRotation * inverse(currentWorldPanning) * viewMatrix;
            shader.SetViewMatrix(viewMatrix);
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
        }

        if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
            cameraVerticalAngle  -= dy * PANNING_ANGULAR_SPEED * dt;
            // Clamp vertical angle to [-85, 85] degrees
            cameraVerticalAngle = std::max(-85.0f, std::min(85.0f, cameraVerticalAngle));
            
            GLfloat theta = radians(cameraHorizontalAngle);
            GLfloat phi = radians(cameraVerticalAngle);
            
            cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
            
            mat4 viewMatrix = shader.GetViewMatrix();
            
            viewMatrix = inverse(currentWorldRotation) * inverse(currentWorldPanning) * viewMatrix;
            currentWorldPanning = lookAt(cameraPosition, cameraPosition + cameraLookAt, cameraUp);
            viewMatrix = currentWorldPanning * currentWorldRotation * viewMatrix; 
            
            shader.SetViewMatrix(viewMatrix);
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
        }
        if(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
            cameraHorizontalAngle -= dx * PANNING_ANGULAR_SPEED * dt;

            if (cameraHorizontalAngle > 360)
                cameraHorizontalAngle -= 360;
            else if (cameraHorizontalAngle < -360)
                cameraHorizontalAngle += 360;
            
            GLfloat theta = radians(cameraHorizontalAngle);
            GLfloat phi = radians(cameraVerticalAngle);
            
            cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
            mat4 viewMatrix = shader.GetViewMatrix();
            viewMatrix = inverse(currentWorldRotation) * inverse(currentWorldPanning) * viewMatrix;
            currentWorldPanning = lookAt(cameraPosition, cameraPosition + cameraLookAt, cameraUp);
            viewMatrix = currentWorldPanning * currentWorldRotation * viewMatrix; 
            shader.SetViewMatrix(viewMatrix);
            // glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &viewMatrix[0][0]);
        }
        if (glfwGetKey(window, GLFW_KEY_M) == GLFW_PRESS) {
            fovy += radians(dy * ZOOM_SPEED * dt);
            if (fovy > pi<GLfloat>())
                fovy = pi<GLfloat>();
            if (fovy < 0)
                fovy = 0;
            perspectiveMatrix = perspective(fovy, ASPECT_RATIO, 0.01f*U, 100.0f*U);
            shader.SetProjectionMatrix(perspectiveMatrix);
            // glUniformMatrix4fv(projectionMatrixLocation, 1, GL_FALSE, &perspectiveMatrix[0][0]);
        }

        if (B_lastState == GLFW_RELEASE && glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS) {
            shader.SetDisplayShadows(!shader.GetDisplayShadows());
        }
        
        spaceLastState = glfwGetKey(window, GLFW_KEY_SPACE);
        X_lastState = glfwGetKey(window, GLFW_KEY_X);
        B_lastState = glfwGetKey(window, GLFW_KEY_B);
        
    }
    // cleanup
    shader.ClearShader();
    // Terminate the program.
    glfwTerminate();
}