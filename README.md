# comp371a1
Environment setup

The following command finds the necessary libraries to create the makefile of the C++ program. 
The `-DCMAKE_EXPORT_COMPILE_COMMANDS=1` is simply a flag to allow the `clangd` extension to work with `cmake` in the linter. As for the `-B build/`, it specifies where to build the files.

```bash
cmake . -B build/ -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

Run the program with the following command:

```bash
cd build/ && make && ./assignment1; cd ../
```
# Sources to build the program
The program design and code was sourced from Udemy.com from an Open GL course.
The following files and program below were used from the course:
```
texture.cpp
texture.h
stb_image.h
```
# Assumptions & Notes to Consider
The rotation of the model is set to be around itself instead of around the y axis, I decided this due to the ambuiguity of the assignment. Rotating around itself is in fact harder than rotating around the y axis since the total translation had to be undone before rotating.

Keys
```
Z: Move towards negative z axis
X: Move towards positive X axis
WASD: translate the model
wasd: rotate model on shoulder
1 + wasd: rotate model on elbow
2 + wasd: rotate model on wrist
x: toggle texture
b: toggle shadows
```


